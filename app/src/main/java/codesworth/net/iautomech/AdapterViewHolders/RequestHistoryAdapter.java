package codesworth.net.iautomech.AdapterViewHolders;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;

import codesworth.net.iautomech.Model.IARequest;
import codesworth.net.iautomech.R;
import codesworth.net.iautomech.Services.Const;
import codesworth.net.iautomech.Services.DataService;

import static codesworth.net.iautomech.R.color.evedark;
import static codesworth.net.iautomech.R.color.theme_dark;

public class RequestHistoryAdapter extends RecyclerView.Adapter<RequestHistoryViewHolder> {

    private ArrayList<IARequest>requests;
    private int type;
    Context context;
    public RequestHistoryAdapter(ArrayList<IARequest> requests,int type,Context context) {
        this.requests = requests;
        this.type = type;
        this.context = context;
    }

    @NonNull
    @Override
    public RequestHistoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View requestview = LayoutInflater.from(parent.getContext()).inflate(R.layout.ia_request_cells,parent,false);
        if (type == Const.IS_TYPEOF_PROVIDER){
            requestview.setMinimumHeight(180);
        }else {
            requestview.setMinimumHeight(140);
        }
        return new RequestHistoryViewHolder(requestview, type,context);
    }

    @Override
    public void onBindViewHolder(@NonNull RequestHistoryViewHolder holder, int position) {
        IARequest request = requests.get(position);
        holder.configureView(request);
    }

    @Override
    public int getItemCount() {
        return requests.size();
    }
}




class RequestHistoryViewHolder extends RecyclerView.ViewHolder {

    TextView requestType;
    TextView carbrand;
    TextView provider;
    TextView status;
    TextView date;
    TextView charge;
    private int type;
    TextView usertype;
    Button statusChanger;
    Context context;

    public RequestHistoryViewHolder(View itemView, int type, Context context) {
        super(itemView);
        this.type = type;
        this.context = context;
        requestType = itemView.findViewById(R.id.h_reqtype);
        carbrand = itemView.findViewById(R.id.h_carbrand);
        provider = itemView.findViewById(R.id.h_serv_provider);
        status = itemView.findViewById(R.id.h_status);
        date = itemView.findViewById(R.id.h_date);
        charge = itemView.findViewById(R.id.h_fee);
        usertype = itemView.findViewById(R.id.user_typpe);
        statusChanger = itemView.findViewById(R.id.statchanger);
        if (type == Const.IS_TYPEOF_PROVIDER){
            statusChanger.setVisibility(View.VISIBLE);

        }else {
            statusChanger.setVisibility(View.GONE);
            statusChanger.setEnabled(false);
        }
    }


    public void configureView(final IARequest request){
        if (type == Const.IS_TYPE_USER){
            requestType.setText(request.getServicetype());
            carbrand.setText(request.getCarbrand());
            provider.setText(request.getServiceProviderName());
            status.setText(request.getStatusString());
            date.setText(request.getDateString());
            charge.setText(request.getCharge());
        }else{
            requestType.setText(request.getServicetype());
            carbrand.setText(request.getCarbrand());
            provider.setText(request.getSenderName());
            status.setText(request.getStatusString());
            date.setText(request.getDateString());
            charge.setText(request.getCharge());
            usertype.setText("Sender Name");
            if (request.getStatus() == 1){
                statusChanger.setText("CHANGE STATUS TO IN_SERVICE");
            }else if (request.getStatus() == 2){
                statusChanger.setText("CHANGE STATUS TO SERVICED");
            }else if (request.getStatus() == 3){
                statusChanger.setText("SERVICED SUCCESFULLY");
                statusChanger.setEnabled(false);
                statusChanger.setBackgroundColor(ContextCompat.getColor(context,R.color.evedark));
            }
            statusChanger.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (request.getStatus() == 1){
                        MaterialDialog dialog = new MaterialDialog.Builder(context)
                                .title("CHANGE REQUEST STATUS")
                                .content("You are about to change the statusof this request to IN_SERVICE, which implies the requested vehicle is been worked on.")
                                .positiveText("YES").negativeText("CANCEL").onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        final MaterialDialog progress = Const.makeLoader("CHANGING STATUS", "Please wait.....",context);
                                        progress.setCanceledOnTouchOutside(false);
                                        DataService.getInstance().changeRequestStatus(request, new DataService.RequestStatusChanged() {
                                            @Override
                                            public void didFinishChangingStatus(boolean status, int newStatus) {
                                                progress.dismiss();
                                               if (status){
                                                   request.setStatus(newStatus);
                                                   RequestHistoryViewHolder.this.status.setText(request.getStatusString());
                                                   statusChanger.setText("CHANGE STATUS TO SERVICED");
                                               }else{
                                                   Const.snackMessage("Failed To Change Status",itemView);
                                               }

                                            }
                                        });
                                    }
                                })
                                .show();
                    }else if (request.getStatus() == 2){
                        MaterialDialog dialog = new MaterialDialog.Builder(context)
                                .title("CHANGE REQUEST STATUS")
                                .content("You are about to change the status of this request to SERVICED, which implies the requested was succesfully worked on.")
                                .positiveText("YES").negativeText("CANCEL").onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        MaterialDialog progress = Const.makeLoader("CHANGING STATUS", "Please wait.....",context);
                                        progress.setCanceledOnTouchOutside(false);
                                        DataService.getInstance().changeRequestStatus(request, new DataService.RequestStatusChanged() {
                                            @Override
                                            public void didFinishChangingStatus(boolean status, int newStatus) {
                                                if (status){
                                                    request.setStatus(newStatus);
                                                    RequestHistoryViewHolder.this.status.setText(request.getStatusString());
                                                    statusChanger.setText("SERVICED");
                                                }else{
                                                    Const.snackMessage("Failed To CHange Status",itemView);
                                                }

                                            }
                                        });
                                    }
                                })
                                .show();
                    }
                }
            });
        }
    }


}
