package codesworth.net.iautomech.Fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.io.IOException;

import codesworth.net.iautomech.Activities.HomeActivity;
import codesworth.net.iautomech.R;
import codesworth.net.iautomech.Services.AuthService;
import codesworth.net.iautomech.Services.Const;
import codesworth.net.iautomech.Services.DataService;


public class SettingsFragment extends Fragment {

    Button signout;
    public SettingsFragment() {
        // Required empty public constructor
    }

    public static SettingsFragment newInstance() {
        SettingsFragment fragment = new SettingsFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        return view;
    }


    void setupViews(View view){
        signout = view.findViewById(R.id.set_sign_out);
        signout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AuthService.auth().signedOut();
                getActivity().getSharedPreferences(Const.class.getName(),0).edit().putInt(Const.USER_TYPE,0).commit();
                getActivity().getSharedPreferences(Const.class.getName(),0).edit().putString(Const.USER_UID,"").commit();
                ((HomeActivity)getActivity()).openSignIn();
                try {
                    DataService.writeObject(getContext(),null);
                } catch (IOException e) {
                    Const.makeAlert("EXCEPTION THROWN",e.getMessage(),getContext(),"DISMISS");
                    e.printStackTrace();
                }
            }
        });
    }

}
