package codesworth.net.iautomech.Fragments;


import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.google.firebase.database.DatabaseError;
import com.wrapp.floatlabelededittext.FloatLabeledEditText;

import java.io.IOException;
import java.util.ArrayList;

import codesworth.net.iautomech.Activities.HomeActivity;
import codesworth.net.iautomech.Model.IAServiceProvider;
import codesworth.net.iautomech.Model.User;
import codesworth.net.iautomech.R;
import codesworth.net.iautomech.Services.AuthService;
import codesworth.net.iautomech.Services.Const;
import codesworth.net.iautomech.Services.DataService;


public class SignUpFragment extends Fragment implements HomeActivity.HomeActivityProtocol {



    Button signupuser;
    Button registerService;
    Button createAccountButt;
    FloatLabeledEditText username;
    FloatLabeledEditText email;
    FloatLabeledEditText password;
    FloatLabeledEditText primaryPhone;
    FloatLabeledEditText secondaryphone;
    FloatLabeledEditText contactname;
    FloatLabeledEditText workaddress;
    FloatLabeledEditText homeadress;
    FloatLabeledEditText wrkshplocation;
    Spinner currentState;
    CheckBox onspotRepairs;
    String[] servicetypes;
    String [] allstates;
    String[] allcarbarands;
    Button carbrandbut;
    Button servicetypebut;
    Integer[] st_indices;
    Integer[] cb_indices;


    MaterialDialog loader;
    CardView normalregView;
    CardView regserviceView;
    Location location;
    private String iacurrentState;
    private boolean hasonspotRepairs;
    private int serviceType;
    private MaterialDialog serviceDialog;
    private MaterialDialog brandialog;
    ArrayList<String> selectedServices;
    ArrayList<String> selectedcarBrands;

    public SignUpFragment() {
        // Required empty public constructor
    }

    public static SignUpFragment newInstance(String param1, String param2) {
        SignUpFragment fragment = new SignUpFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }

        ((HomeActivity)getActivity()).locationlistener = this;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sign_up, container, false);
        setUpView(view);

        normalregView = view.findViewById(R.id.createacc_card);
        regserviceView = view.findViewById(R.id.servicereg_card);
        signupuser = view.findViewById(R.id.reguser);

        signupuser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createUserAccount();
                //normalregView.setVisibility(View.GONE);
                //regserviceView.setVisibility(View.VISIBLE);

            }
        });
        return view;
    }



    private void setUpView(View view){
        servicetypes = getResources().getStringArray(R.array.service_type);
        allstates = getResources().getStringArray(R.array.states);
        allcarbarands = getResources().getStringArray(R.array.car_brands);
        onspotRepairs = view.findViewById(R.id.onspotcheck);
        registerService = view.findViewById(R.id.reg_service);
        username = view.findViewById(R.id.floatLabeledusername);
        email =  view.findViewById(R.id.floatLabeledemail);
        password = view.findViewById(R.id.floatLabeledpassword);
        primaryPhone = view.findViewById(R.id.floatLabeledphone1);
        secondaryphone = view.findViewById(R.id.floatLabeledphone2);
        workaddress = view.findViewById(R.id.floatLabeledwksaddress);
        homeadress = view.findViewById(R.id.floatLabeledhomaddr);
        contactname = view.findViewById(R.id.floatLabeledcontname);
        wrkshplocation = view.findViewById(R.id.floatLabeledwkloc);
        servicetypebut = view.findViewById(R.id.serviceoffered);
        carbrandbut = view.findViewById(R.id.carbrands);
        currentState = view.findViewById(R.id.currentstate);
        if (location == null){
            ((HomeActivity)getActivity()).startLocationServices();
        }
        ArrayAdapter<CharSequence> stateadapter = ArrayAdapter.createFromResource(getContext(),
                R.array.states, android.R.layout.simple_spinner_item);
        stateadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        currentState.setAdapter(stateadapter);


        currentState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                iacurrentState = allstates[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        registerService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()){
                    normalregView.setVisibility(View.GONE);
                    regserviceView.setVisibility(View.VISIBLE);
                }
            }
        });
        createAccountButt = view.findViewById(R.id.finishreg);
        createAccountButt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (location != null && validateService()){
                    new MaterialDialog.Builder(getContext())
                            .title("IMPORTANT")
                            .content(R.string.location_info_warn)
                            .positiveText("CONFIRM").onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            createServiceAccount();
                        }
                    }).onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            getActivity().finishAffinity();
                        }
                    })
                            .negativeText("DISMISS")
                            .show();
                }else{
                    Const.makeAlert("GPS ERROR", "Unable to obtain Location coordiantes. Please turn on location services to allow IAUTOMECH to access your location",getContext(),"OK");
                }
            }
        });

        carbrandbut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                brandialog = new MaterialDialog.Builder(getContext())
                        .title("CAR BRANDS SUPPORTED")
                        .items(R.array.car_brands).itemsCallbackMultiChoice(cb_indices, new MaterialDialog.ListCallbackMultiChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, Integer[] which, CharSequence[] text) {
                        cb_indices = which;
                        makebrandArray(which);
                        return true;
                    }
                }).positiveText("CONFIRM").show();
            }
        });

        servicetypebut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                serviceDialog = new MaterialDialog.Builder(getContext())
                        .title("CAR BRANDS SUPPORTED")
                        .items(R.array.service_type).itemsCallbackMultiChoice(st_indices, new MaterialDialog.ListCallbackMultiChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, Integer[] which, CharSequence[] text) {
                        st_indices = which;
                        makeserviceArray(which);
                        return true;
                    }
                }).positiveText("CONFIRM").show();
            }
        });




    }

    private boolean validate(){
        if (username.getEditText().getText().length() > 0 && email.getEditText().getText().toString().contains("@") && password.getEditText().getText().length() > 5 && primaryPhone.getEditText().getText().length() == 10){
            return true;
        }
        Const.makeAlert("Invalid Fields", "Please enter a valid email address and a password longer than 5 characters and a valid phone number",getContext(),"OK");

        return false;
    }

    private void createUserAccount(){
        if (validate()){
            loader = Const.makeLoader("Creating User", "Please wait.....", getContext());
            loader.setCanceledOnTouchOutside(false);
            AuthService.auth().signUpWithEmailPass(email.getEditText().getText().toString(), password.getEditText().getText().toString(), new AuthService.DidSignUp() {
                @Override
                public void sighupCompleted(final String uid) {
                    User user = new User(uid,username.getEditText().getText().toString(),primaryPhone.getEditText().getText().toString(),secondaryphone.getEditText().getText().toString(),email.getEditText().getText().toString(),"link");
                    DataService.getInstance().createUser(user, new DataService.CompletionHandler() {
                        @Override
                        public void completed(boolean succesfully) {
                            if (succesfully){
                                loader.dismiss();
                                getActivity().getSharedPreferences(Const.class.getName(),0).edit().putString(Const.USER_UID,uid).commit();
                                DataService.getInstance().getUserTyppe(uid, new DataService.AuthCompletion() {
                                    @Override
                                    public void onComplete(Object object, int type) {
                                        getActivity().getSharedPreferences(Const.class.getName(),0).edit().putInt(Const.USER_TYPE,type).commit();
                                        try {
                                            DataService.writeObject(getContext(),object);
                                        } catch (IOException e) {
                                            Const.makeAlert("IOEXCEPTION RAISED", e.getLocalizedMessage(),getContext(),"DISMISS");
                                            e.printStackTrace();
                                        }
                                        ((HomeActivity)getActivity()).openHome();
                                    }
                                });
                            }else {
                                loader.dismiss();
                                Const.makeAlert("Error", "Error occurred whiles creating user, Please check network connection", getContext(), "OK");
                            }
                        }
                    });
                }
            }, new AuthService.FailedSignIn() {
                @Override
                public void failedSignUp(Exception e) {
                    Const.makeAlert("Error", "Error occurred whiles creating user, Please check network connection. Please sign in if you have already created an account", getContext(), "OK");
                }
            });
        }
    }

    private boolean validateService(){
        if (contactname.getEditText().getText().length() > 0 && workaddress.getEditText().getText().length() > 0 && wrkshplocation.getEditText().getText().length() > 0){
            if (selectedServices != null && selectedcarBrands != null){
                return true;
            }else{
                Const.makeAlert("Invalid Selections", "Please ensure you have selected supported car brands and services", getContext(),"OK");
                return false;
            }
        }
        Const.makeAlert("Invalid Service Fields", "Please ensure you have filled all fields correctly", getContext(),"OK");
        return false;
    }

    private void createServiceAccount(){
        if (validateService()){
            loader = Const.makeLoader("Creating Service Provider", "Please wait.....", getContext());
            loader.setCanceledOnTouchOutside(false);
            AuthService.auth().signUpWithEmailPass(email.getEditText().getText().toString(), password.getEditText().getText().toString(), new AuthService.DidSignUp() {
                @Override
                public void sighupCompleted(final String uid) {
                    hasonspotRepairs = onspotRepairs.isChecked();
                    IAServiceProvider serviceProvider = new IAServiceProvider(uid,username.getEditText().getText().toString(),contactname.getEditText().getText().toString(),workaddress.getEditText().getText().toString(),homeadress.getEditText().getText().toString(), iacurrentState,wrkshplocation.getEditText().getText().toString(),primaryPhone.getEditText().getText().toString(),secondaryphone.getEditText().getText().toString(),selectedcarBrands,selectedServices, hasonspotRepairs,location.getLatitude(),location.getLongitude());
                    DataService.getInstance().createServiceProvider(serviceProvider, new DataService.CompletionHandler() {
                        @Override
                        public void completed(boolean succesfully) {

                            GeoFire geoFire = new GeoFire(DataService.getInstance().getGeoRef());
                            geoFire.setLocation(uid, new GeoLocation(location.getLatitude(), location.getLongitude()), new GeoFire.CompletionListener() {
                                @Override
                                public void onComplete(String key, DatabaseError error) {
                                    if (error == null){
                                        loader.dismiss();
                                        Const.snackMessage("Service Account Created",getView());
                                        getActivity().getSharedPreferences(Const.class.getName(),0).edit().putString(Const.USER_UID,uid).commit();
                                        DataService.getInstance().getUserTyppe(uid, new DataService.AuthCompletion() {
                                            @Override
                                            public void onComplete(Object object, int type) {
                                                getActivity().getSharedPreferences(Const.class.getName(),0).edit().putInt(Const.USER_TYPE,type).commit();
                                                try {
                                                    DataService.writeObject(getContext(),object);
                                                } catch (IOException e) {
                                                    Const.makeAlert("IOEXCEPTION RAISED", e.getLocalizedMessage(),getContext(),"DISMISS");
                                                    e.printStackTrace();
                                                }
                                                ((HomeActivity)getActivity()).openHome();
                                            }
                                        });
                                    }else{
                                        Const.snackMessage(error.getMessage(),getView());
                                    }
                                }
                            });
                        }
                    });
                }
            }, new AuthService.FailedSignIn() {
                @Override
                public void failedSignUp(Exception e) {
                    loader.dismiss();
                    Const.snackMessage(e.getMessage(),getView());
                }
            });
        }
    }

    void makebrandArray(Integer[] indices){
        selectedcarBrands = new ArrayList<>();
        for (Integer i:indices) {
            String s = allcarbarands[i];
            selectedcarBrands.add(s);
        }
    }

    void makeserviceArray(Integer[] indices){
        selectedServices = new ArrayList<>();
        for (Integer i:indices) {
            String s = servicetypes[i];
            selectedServices.add(s);
        }
    }

    @Override
    public void didFinishGettingLocation(Location location) {
        this.location = location;
    }
}
