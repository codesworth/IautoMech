package codesworth.net.iautomech.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.afollestad.materialdialogs.MaterialDialog;
import com.wrapp.floatlabelededittext.FloatLabeledEditText;

import java.io.IOException;
import java.util.HashMap;

import codesworth.net.iautomech.Activities.HomeActivity;
import codesworth.net.iautomech.R;
import codesworth.net.iautomech.Services.AuthService;
import codesworth.net.iautomech.Services.Const;
import codesworth.net.iautomech.Services.DataService;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SignInFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SignInFragment extends Fragment {


    FloatLabeledEditText email;
    FloatLabeledEditText password;
    Button login;
    Button register;
    private MaterialDialog loader;

    public SignInFragment() {
        // Required empty public constructor
    }


    public static SignInFragment newInstance(String param1, String param2) {
        SignInFragment fragment = new SignInFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sign_in, container, false);
        setupViews(view);
        return view;
    }

    void setupViews(View view){
        email = view.findViewById(R.id.fledusername);
        password = view.findViewById(R.id.fledpassword);
        login = view.findViewById(R.id.loginbutt);
        register = view.findViewById(R.id.registerbut);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (email.getEditText().toString().contains("@") && password.getEditText().getText().toString().length() > 0){
                    loader = Const.makeLoader("Creating User", "Please wait.....", getContext());
                    loader.setCanceledOnTouchOutside(false);
                    AuthService.auth().signInWithEmailPass(email.getEditText().getText().toString(), password.getEditText().getText().toString(), new AuthService.Authenticted() {
                        @Override
                        public void didauthenticate(String uid,Object object, int type) {
                            loader.dismiss();
                            getActivity().getSharedPreferences(Const.class.getName(),0).edit().putString(Const.USER_UID,uid).commit();
                            getActivity().getSharedPreferences(Const.class.getName(),0).edit().putInt(Const.USER_TYPE,type).commit();
                            try {
                                DataService.writeObject(getContext(),object);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            ((HomeActivity)getActivity()).openHome();
                        }
                    }, new AuthService.FailedSignIn() {
                        @Override
                        public void failedSignUp(Exception e) {

                        }
                    });
                }
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity)getActivity()).openSignupFragment();
            }
        });
    }

}
