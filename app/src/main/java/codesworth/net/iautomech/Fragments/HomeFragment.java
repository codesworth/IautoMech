package codesworth.net.iautomech.Fragments;


import android.graphics.Camera;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import codesworth.net.iautomech.Activities.HomeActivity;
import codesworth.net.iautomech.R;
import codesworth.net.iautomech.Services.Const;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment implements OnMapReadyCallback, HomeActivity.HomeActivityProtocol {

    GoogleMap mMap;
    MarkerOptions myMarker;
    Camera camera;
    Button automech;
    Button options;
    Button tow;
    Button diagnostic;
    Button automechReqs;
    Button towReqs;
    Button settings;
    Location location;
    boolean locationFired = false;
    public int type;
    View view;

    public HomeFragment() {
        // Required empty public constructor
    }


    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }

        ((HomeActivity)getActivity()).locationlistener = this;
        ((HomeActivity)getActivity()).getUserDetails();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        type = getActivity().getSharedPreferences(Const.class.getName(),0).getInt(Const.USER_TYPE,0);
        if (type == Const.IS_TYPEOF_PROVIDER){
            view = inflater.inflate(R.layout.iaservice_provider_home_layout, container, false);
        }else{
            view = inflater.inflate(R.layout.fragment_home, container, false);
            setupUserViewViews();
        }




        return view;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        setuserLocation(location);
    }

    void setupUserViewViews(){

            tow = view.findViewById(R.id.tow);
            tow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((HomeActivity)getActivity()).openOptions(true);
                }
            });
            automech = view.findViewById(R.id.automech);
            options = view.findViewById(R.id.options);
            diagnostic = view.findViewById(R.id.diagnostics);
            automech.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ((HomeActivity)getActivity()).openOptions(false);
                }
            });
            if (!locationFired && ((HomeActivity)getActivity()).location != null){
                this.location = ((HomeActivity)getActivity()).location;

            }
        final SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }




    public void setuserLocation(Location location){
    if (location != null){
        LatLng mylocation = new LatLng(location.getLatitude(),location.getLongitude());
        myMarker = new MarkerOptions().position(mylocation).title("Me");
        mMap.addMarker(myMarker);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mylocation, 15));
    }

    }

    @Override
    public void didFinishGettingLocation(Location location) {
        this.location = location;
        setuserLocation(location);
        locationFired = true;
    }


    void setupProviderview(){
        automechReqs = view.findViewById(R.id.automecch_reqs);
        towReqs = view.findViewById(R.id.tow_reqs);
        settings = view.findViewById(R.id.pr_setings);

        automechReqs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        towReqs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity)getActivity()).openRequestHistorry(true);
            }
        });

        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity)getActivity()).openSettings();
            }
        });
    }
}
