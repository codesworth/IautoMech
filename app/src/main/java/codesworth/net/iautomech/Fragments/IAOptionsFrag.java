package codesworth.net.iautomech.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import codesworth.net.iautomech.Activities.HomeActivity;
import codesworth.net.iautomech.Model.IAServiceProvider;
import codesworth.net.iautomech.R;


public class IAOptionsFrag extends Fragment implements View.OnClickListener {

    private static final String ARG_PARAM_TOW = "towing";
    Spinner brandspinner;
    Spinner servicespinner;
    //CardView mapcard;
    String[] serv_types;
    String[] brands;
    private String uid;
    //GeoFire geoFire;
    CardView radiuscard;
    CardView requestCard;
    Spinner  radius_spinner;
    //TextView providerName;
    TextView servicetype;
    TextView car_brand;
    //Button placeRequest;
    String selectedService;
    Button req_dismiss;
    private int radius = 5;
    Button findshops;
    boolean isTowing;
    CardView servicetypecard;
    TextView saservice;
    private String selectedBrand;
    IAServiceProvider selectedProvider;


    public IAOptionsFrag() {
        // Required empty public constructor
    }


    public static IAOptionsFrag newInstance(boolean isTowing) {
        IAOptionsFrag fragment = new IAOptionsFrag();
        Bundle args = new Bundle();
        args.putBoolean(ARG_PARAM_TOW,isTowing);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            isTowing = getArguments().getBoolean(ARG_PARAM_TOW);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_iaoptions, container, false);
        setupViews(view);
        ((HomeActivity)getActivity()).getSupportActionBar().setTitle("PLACE REQUEST");
        return view;
    }

    void setupViews(View view){
        saservice = view.findViewById(R.id.servicened);
        servicetypecard = view.findViewById(R.id.service_type_card);
        servicespinner = view.findViewById(R.id.servicespinner);
        brandspinner = view.findViewById(R.id.brandspinner);
        brands = getResources().getStringArray(R.array.car_brands);
        serv_types = getResources().getStringArray(R.array.service_type_ntow);

        //req_dismiss = view.findViewById(R.id.req_dismiss);
        findshops = view.findViewById(R.id.findaushop);

        radiuscard = view.findViewById(R.id.area_radius);

        radius_spinner = view.findViewById(R.id.radius);
        //providerName = view.findViewById(R.id.req_providername);
        servicetype = view.findViewById(R.id.distance);
        car_brand = view.findViewById(R.id.req_carbrand);
        //placeRequest = view.findViewById(R.id.confirm_auto_request);
        radiuscard.setVisibility(View.VISIBLE);


        ArrayAdapter<CharSequence> radiusAdapter = ArrayAdapter.createFromResource(getContext(),
                R.array.radius_array, android.R.layout.simple_spinner_item);
        radiusAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        radius_spinner.setAdapter(radiusAdapter);
        radius_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0){
                    radius = selectedRadius(position);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<CharSequence> bradapter = ArrayAdapter.createFromResource(getContext(),
                R.array.adapter_car_brands, android.R.layout.simple_spinner_item);
        bradapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        brandspinner.setAdapter(bradapter);
        ArrayAdapter<CharSequence> seradapter = ArrayAdapter.createFromResource(getContext(),
                R.array.adapter_service_type, android.R.layout.simple_spinner_item);
        seradapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        servicespinner.setAdapter(seradapter);

        servicespinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0){

                    selectedService = serv_types[position];

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        brandspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0){
                    selectedBrand = brands[position];
                }
            }
        });
        if(isTowing){
            saservice.setVisibility(View.GONE);
            servicetypecard.setVisibility(View.GONE);
        }
        findshops.setOnClickListener(this);

    }



    private Integer selectedRadius(int position){
        String [] radii = getResources().getStringArray(R.array.radius_array);
        String radius = radii[position];
        switch (radius){
            case "5":
                return 5;
            case "7":
                return 7;
            case "10":
                return 10;
            case "15":
                return 15;
            case "20":
                return 20;
            case "50":
                return 50;
            default:
                return 5;
        }
    }

    @Override
    public void onClick(View v) {
        if (isTowing){
            if (selectedBrand != null){
                ((HomeActivity)getActivity()).openTow(selectedBrand,radius);
            }
        }else{
            if (selectedBrand != null && selectedService != null){
                ((HomeActivity)getActivity()).openAutoMech(selectedService,selectedBrand,radius);
            }
        }
    }
}
