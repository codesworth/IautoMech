package codesworth.net.iautomech.Fragments;


import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DatabaseError;


import java.util.ArrayList;
import java.util.HashMap;

import codesworth.net.iautomech.Activities.HomeActivity;
import codesworth.net.iautomech.Model.IARequest;
import codesworth.net.iautomech.Model.IAServiceProvider;
import codesworth.net.iautomech.R;
import codesworth.net.iautomech.Services.Const;
import codesworth.net.iautomech.Services.DataService;
import codesworth.net.iautomech.Services.IADirectionData;


public class IATOWFrag extends Fragment implements OnMapReadyCallback, HomeActivity.HomeActivityProtocol,IADirectionData.AsyncTaskComplete {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public static final double BASE_FEE = 2000;
    public static final double MIN_FEE = 0.83;
    public static final double PERR_KM_FEE = 0.5;
    public static final double BOOKING_FEE = 1000;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    CardView requestCard;
    TextView estimatedcost;
    TextView towDistance;
    TextView providerName;
    TextView durationText;
    TextView servicetype;
    TextView car_brand;
    Button placeRequest;
    Button req_dismiss;
    private String uid;
    GeoFire geoFire;
    HashMap<String, String > markers;
    ArrayList<IAServiceProvider> serviceProviders;
    private String selectedBrand;
    IAServiceProvider selectedProvider;
    private Location location;
    private GoogleMap mMap;
    private MarkerOptions myMarker;
    private String selectedService;
    private int radius = 5;
    //LatLng tslatlng;
    Object[] datatransfer;
    private MaterialDialog progress;
    private MaterialDialog alert;
    private String senderName;

    public IATOWFrag() {
        // Required empty public constructor
    }


    public static IATOWFrag newInstance(String brand, int radius) {
        IATOWFrag fragment = new IATOWFrag();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, brand);
        args.putInt(ARG_PARAM2, radius);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            selectedBrand = getArguments().getString(ARG_PARAM1);
            radius = getArguments().getInt(ARG_PARAM2);
        }
        if (((HomeActivity)getActivity()).location != null){
            location = ((HomeActivity)getActivity()).location;
        }

        senderName = ((HomeActivity)getActivity()).user.getName();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_iatow, container, false);
        serviceProviders = new ArrayList<>();
        uid = getActivity().getSharedPreferences(Const.class.getName(),0).getString(Const.USER_UID,"");
        ((HomeActivity)getActivity()).getSupportActionBar().setTitle("TOW");
        markers = new HashMap<>();
        setupViews(view);
        final SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        return view;
    }


    void setupViews(View view){

        durationText = view.findViewById(R.id.estim_dura);
        towDistance = view.findViewById(R.id.distance);
        estimatedcost = view.findViewById(R.id.estim_cost);
        req_dismiss = view.findViewById(R.id.req_dismiss);
        req_dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestCard.setVisibility(View.GONE);
                Animation animation = AnimationUtils.loadAnimation(getContext(),R.anim.slide_down_left);
                requestCard.setAnimation(animation);

            }
        });

        requestCard = view.findViewById(R.id.request_card);

        providerName = view.findViewById(R.id.req_providername);
        servicetype = view.findViewById(R.id.req_service_type);
        car_brand = view.findViewById(R.id.req_carbrand);
        placeRequest = view.findViewById(R.id.confirm_auto_request);
        selectedService = "TOW";

        requestCard.setVisibility(View.GONE);

        placeRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert = Const.makeAlert("TOWING REQUEST", "You are about to place a towing request. Be informed that estimated cost is likely to change. Charges apply per duration and distance which may change during course of operation. Do you want to continue?", getContext(),"YES");
               alert.getBuilder().negativeText("CANCEL").onPositive(new MaterialDialog.SingleButtonCallback() {
                   @Override
                   public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                    progress = Const.makeLoader("PLACING REQUEST", "Please wait.....",getContext());
                    progress.setCanceledOnTouchOutside(false);
                    placeARequest();
                   }
               });
            }
        });



    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (location != null){
            setuserLocation(location);
        }
        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                if (!marker.getTitle().contentEquals("Me")){
                    IAServiceProvider provider = chooseServiceProvider(marker.getTitle());
                    providerName.setText(provider.getName());
                    servicetype.setText("TOWING");
                    car_brand.setText(selectedBrand);
                    requestCard.setVisibility(View.VISIBLE);
                    Animation anim = AnimationUtils.loadAnimation(getContext(),R.anim.slide_up_left);

                    requestCard.startAnimation(anim);
                    selectedProvider = provider;


                    datatransfer = new Object[2];
                    IADirectionData ddata = new IADirectionData();
                    ddata.listener = IATOWFrag.this;
                    datatransfer[0] = mMap;
                    datatransfer[1] = getDirectionsUrl(marker.getPosition());
                    progress = Const.makeLoader("Calculating.....", "Please wait",getContext());
                    progress.setCanceledOnTouchOutside(false);
                    ddata.execute(datatransfer);



                }
            }
        });
        geoFire = new GeoFire(DataService.getInstance().getGeoRef());
        beginQuery(radius);

    }

    void filterProviders(String service, ArrayList<IAServiceProvider>providers){
        ArrayList<IAServiceProvider> sps = new ArrayList<>();
        for (IAServiceProvider provider: providers){
            if (provider.getSupportedServices().contains(service)){
                sps.add(provider);
            }
        }
    }

    void placeARequest(){
        String id = DataService.getInstance().getDocumentID();
        IARequest request = new IARequest(id,uid,selectedProvider.getId(),selectedBrand,selectedService,1,System.currentTimeMillis(),0,selectedProvider.getName(),senderName);
        //final MaterialDialog loader = Const.makeLoader("Sending Request...","Please wait...",getContext());
        //loader.setCanceledOnTouchOutside(false);
        DataService.getInstance().placeRequest(request,uid, new DataService.CompletionHandler() {
            @Override
            public void completed(boolean succesfully) {
                if (succesfully){
                    progress.dismiss();
                    Const.makeAlert("Succesful", "Request was succesfully placed. You will be contacted for more info",getContext(),"OK");
                    getActivity().getSupportFragmentManager().popBackStack();
                }else{
                    progress.dismiss();
                    Const.makeAlert("OOPS", "Something went wrong. Please check network connection and place request again",getContext(),"DISMISS");
                }
            }
        });
    }

    void beginQuery(double radius){
        GeoLocation loca = new GeoLocation(location.getLatitude(),location.getLongitude());
        GeoQuery query = geoFire.queryAtLocation(loca , radius);
        query.addGeoQueryEventListener(new GeoQueryEventListener() {
            @Override
            public void onKeyEntered(String key, GeoLocation location) {
                Log.d("SERVICE PROVIDER", "The key is actually "+ key);
                DataService.getInstance().fetchServiceProvider(key, new DataService.Completion() {
                    @Override
                    public void didFinish(Object object) {
                        if (object.getClass() == IAServiceProvider.class){
                            IAServiceProvider provider = (IAServiceProvider)object;
                            //if (provider.getSupportedServices().contains(selectedService)){
                                setServiceProviderLocation(provider);
                                serviceProviders.add(provider);
//                                if (!serviceProviders.contains(provider)){
//                                    serviceProviders.add(provider);
//
//                                }
                            //}
                        }
                    }
                });
            }

            @Override
            public void onKeyExited(String key) {

            }

            @Override
            public void onKeyMoved(String key, GeoLocation location) {

            }

            @Override
            public void onGeoQueryReady() {

            }

            @Override
            public void onGeoQueryError(DatabaseError error) {

            }
        });
    }

    public void setServiceProviderLocation(IAServiceProvider provider){
        if (!markers.containsKey(provider.getId())){
            LatLng mylocation = new LatLng(provider.getLatitude(),provider.getLongitude());
            MarkerOptions procidermakr = new MarkerOptions().position(mylocation).title(provider.getName());
            mMap.addMarker(procidermakr);
            markers.put(provider.getId(),provider.getName());
        }



    }

    public void setuserLocation(Location location){
        LatLng mylocation = new LatLng(location.getLatitude(),location.getLongitude());
        myMarker = new MarkerOptions().position(mylocation).title("Me");
        mMap.addMarker(myMarker);
        Log.d("LOCATION", "The location is " + mylocation);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mylocation, 15));

    }

    IAServiceProvider chooseServiceProvider(String name){
        for (IAServiceProvider provider:serviceProviders){
            if (provider.getName().equalsIgnoreCase(name)){
                return  provider;
            }
        }
        return  null;
    }


    @Override
    public void didFinishGettingLocation(Location location) {
        this.location = location;
    }



    public String makeURL (double sourcelat, double sourcelog, double destlat, double destlog ){
        StringBuilder urlString = new StringBuilder();
        urlString.append("https://maps.googleapis.com/maps/api/directions/json");
        urlString.append("?origin=");// from
        urlString.append(Double.toString(sourcelat));
        urlString.append(",");
        urlString
                .append(Double.toString( sourcelog));
        urlString.append("&destination=");// to
        urlString
                .append(Double.toString( destlat));
        urlString.append(",");
        urlString.append(Double.toString(destlog));
        urlString.append("&sensor=false&mode=driving&alternatives=true");
        urlString.append("&key=SERVER-KEY");
        return urlString.toString();
    }

    String getDirectionsUrl(LatLng tslatlng){

        StringBuilder gdirurl = new StringBuilder("https://maps.googleapis.com/maps/api/directions/json?");
        gdirurl.append("origin="+location.getLatitude() + ","+location.getLongitude());
        gdirurl.append("&destination=" + tslatlng.latitude+","+tslatlng.longitude);
        gdirurl.append("&key=AIzaSyBwSroDPVm_5cjenHy8OofC53P-xaaXGF0");

        return gdirurl.toString();

    }

    @Override
    public void finishedTask(HashMap<String, Object> result) {
        String dist = (String) result.get(Const.JSON_KEY_DISTANCE);
        String duration = (String) result.get(Const.JSON_KEY_DURATION);
        towDistance.setText(dist.toUpperCase());
        double distanceval = (double) result.get(Const.DIST_VAL);
        double durval = (double)result.get(Const.DUR_VAL);
        double fee = calculateFare(distanceval,durval);
        estimatedcost.setText("N "+fee);
        progress.dismiss();

    }

    double calculateFare(double distance,double duration){
        double distfare = distance * PERR_KM_FEE;
        double durfare = duration * MIN_FEE;
        double total = BASE_FEE + distfare + durfare + BOOKING_FEE;
        return total;
    }


}
