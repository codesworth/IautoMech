package codesworth.net.iautomech.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import codesworth.net.iautomech.Activities.HomeActivity;
import codesworth.net.iautomech.R;


public class OptionsFrag extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private ListView listView;
    private ArrayAdapter<String> adapter;

    public OptionsFrag() {
        // Required empty public constructor
    }

    public static OptionsFrag newInstance(String param1, String param2) {
        OptionsFrag fragment = new OptionsFrag();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_options, container, false);

        listView = (ListView) view.findViewById(R.id.options);
        adapter = new ArrayAdapter<String >(this.getActivity(), R.layout.simple_row,getResources().getStringArray(R.array.options));
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case 0:
                        break;
                    case 1:
                        ((HomeActivity)getActivity()).openSettings();
                        break;
                    case 2:
                        ((HomeActivity)getActivity()).openRequestHistorry(false);
                        break;
                    case 3:
                        break;
                    default:
                        break;
                }
            }
        });

        return view;
    }



}
