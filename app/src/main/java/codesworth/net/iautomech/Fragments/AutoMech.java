package codesworth.net.iautomech.Fragments;


import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DatabaseError;

import java.util.ArrayList;
import java.util.HashMap;

import codesworth.net.iautomech.Activities.HomeActivity;
import codesworth.net.iautomech.Model.IARequest;
import codesworth.net.iautomech.Model.IAServiceProvider;
import codesworth.net.iautomech.R;
import codesworth.net.iautomech.Services.Const;
import codesworth.net.iautomech.Services.DataService;


public class AutoMech extends Fragment implements OnMapReadyCallback, HomeActivity.HomeActivityProtocol {

    private static final String ARG_PARAM_SBRAND = "brand";
    private static final String ARG_PARAM_RADIUS = "radius";
    private static final String ARG_PARAM_SERV = "service";



    MarkerOptions myMarker;
    GoogleMap mMap;
    LatLng myLatlng;
    GeoFire geoFire;
    private String uid;
    String senderName;
    CardView requestCard;

    TextView providerName;
    TextView servicetype;
    TextView car_brand;
    Button placeRequest;
    String selectedService;
    Button req_dismiss;
    private int radius = 5;
    HashMap<String, String >markers;
    ArrayList<IAServiceProvider> serviceProviders;
    private String selectedBrand;
    IAServiceProvider selectedProvider;
    private Location location;

    public AutoMech() {
        // Required empty public constructor
    }


    public static AutoMech newInstance(String service, String brand, int radius) {
        AutoMech fragment = new AutoMech();
        Bundle args = new Bundle();
        //args.putDouble(ARG_PARAM_LAT,lat);
        //args.putDouble(ARG_PARAM_LONG, longt);
        args.putString(ARG_PARAM_SBRAND, brand);
        args.putString(ARG_PARAM_SERV, service);
        args.putInt(ARG_PARAM_RADIUS, radius);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

            selectedBrand = getArguments().getString(ARG_PARAM_SBRAND);
            selectedService = getArguments().getString(ARG_PARAM_SERV);
            radius = getArguments().getInt(ARG_PARAM_RADIUS);
            //myLatlng = new LatLng(lat,lon);
        }

        ((HomeActivity)getActivity()).locationlistener = this;
        if (((HomeActivity)getActivity()).location != null){
           location = ((HomeActivity)getActivity()).location;
        }
        
        ((HomeActivity) getActivity()).getSupportActionBar().setTitle("AUTOMECH");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_auto_mech, container, false);
        serviceProviders = new ArrayList<>();
        uid = getActivity().getSharedPreferences(Const.class.getName(),0).getString(Const.USER_UID,"");
        markers = new HashMap<>();
        setupViews(view);
        final SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        return  view;
    }


    void setupViews(View view){

        req_dismiss = view.findViewById(R.id.req_dismiss);
        req_dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestCard.setVisibility(View.GONE);
                Animation animation = AnimationUtils.loadAnimation(getContext(),R.anim.slide_down_left);
                requestCard.setAnimation(animation);

            }
        });

        requestCard = view.findViewById(R.id.req_con_card);

        providerName = view.findViewById(R.id.estim_dura);
        servicetype = view.findViewById(R.id.distance);
        car_brand = view.findViewById(R.id.req_carbrand);
        placeRequest = view.findViewById(R.id.confirm_auto_request);


        requestCard.setVisibility(View.GONE);

        placeRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id = DataService.getInstance().getDocumentID();
                IARequest request = new IARequest(id,uid,selectedProvider.getId(),selectedBrand,selectedService,1,System.currentTimeMillis(),0,selectedProvider.getName(),senderName);
                final MaterialDialog loader = Const.makeLoader("Sending Request...","Please wait...",getContext());
                loader.setCanceledOnTouchOutside(false);
                DataService.getInstance().placeRequest(request,uid, new DataService.CompletionHandler() {
                    @Override
                    public void completed(boolean succesfully) {
                        if (succesfully){
                            loader.dismiss();
                            Const.makeAlert("Succesful", "Request was succesfully placed. You will be contacted for more info",getContext(),"OK");
                            getActivity().getSupportFragmentManager().popBackStack();
                        }else{
                            loader.dismiss();
                            Const.makeAlert("OOPS", "Something went wrong. Please check network connection and place request again",getContext(),"DISMISS");
                        }
                    }
                });
            }
        });



    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (location != null){
            setuserLocation(location);
        }
        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                if (!marker.getTitle().contentEquals("Me")){
                    IAServiceProvider provider = chooseServiceProvider(marker.getTitle());
                    providerName.setText(provider.getName());
                    servicetype.setText(selectedService);
                    car_brand.setText(selectedBrand);
                    requestCard.setVisibility(View.VISIBLE);
                    Animation anim = AnimationUtils.loadAnimation(getContext(),R.anim.slide_up_left);
                    requestCard.startAnimation(anim);
                    selectedProvider = provider;
                }
            }
        });

        geoFire = new GeoFire(DataService.getInstance().getGeoRef());
        beginQuery();
        //mapcard.setVisibility(View.VISIBLE);

    }


    public void setuserLocation(Location location){
        LatLng mylocation = new LatLng(location.getLatitude(),location.getLongitude());
        myMarker = new MarkerOptions().position(mylocation).title("Me");
        mMap.addMarker(myMarker);
        Log.d("LOCATION", "The location is " + mylocation);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mylocation, 15));

    }

    public void setuserLocation(LatLng latLng){
        //LatLng mylocation = new LatLng(location.getLatitude(),location.getLongitude());
        myMarker = new MarkerOptions().position(latLng).title("Me");
        mMap.addMarker(myMarker);
        Log.d("LOCATION", "The location is " + latLng);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));

    }

    public void setServiceProviderLocation(IAServiceProvider provider){
        if (!markers.containsKey(provider.getId())){
            LatLng mylocation = new LatLng(provider.getLatitude(),provider.getLongitude());
            MarkerOptions procidermakr = new MarkerOptions().position(mylocation).title(provider.getName());
            mMap.addMarker(procidermakr);
            markers.put(provider.getId(),provider.getName());
        }



    }

    @Override
    public void didFinishGettingLocation(Location location) {
        setuserLocation(location);
        myLatlng = new LatLng(location.getLatitude(),location.getLongitude());
    }

    void beginQuery(){
        GeoLocation location = new GeoLocation(this.location.getLatitude(),this.location.getLongitude());
        GeoQuery query = geoFire.queryAtLocation(location, radius);
        query.addGeoQueryEventListener(new GeoQueryEventListener() {
            @Override
            public void onKeyEntered(String key, GeoLocation location) {
                DataService.getInstance().fetchServiceProvider(key, new DataService.Completion() {
                    @Override
                    public void didFinish(Object object) {
                        if (object.getClass() == IAServiceProvider.class){
                            IAServiceProvider provider = (IAServiceProvider)object;
                            if (provider.getSupportedServices().contains(selectedService)){
                                setServiceProviderLocation(provider);
                                serviceProviders.add(provider);
//                                if (!serviceProviders.contains(provider)){
//                                    serviceProviders.add(provider);
//
//                                }
                            }
                        }
                    }
                });
            }

            @Override
            public void onKeyExited(String key) {

            }

            @Override
            public void onKeyMoved(String key, GeoLocation location) {

            }

            @Override
            public void onGeoQueryReady() {

            }

            @Override
            public void onGeoQueryError(DatabaseError error) {

            }
        });
    }



    IAServiceProvider chooseServiceProvider(String name){
        for (IAServiceProvider provider:serviceProviders){
            if (provider.getName().equalsIgnoreCase(name)){
                return  provider;
            }
        }
        return  null;
    }

    void filterProviders(String service, ArrayList<IAServiceProvider>providers){
        ArrayList<IAServiceProvider> sps = new ArrayList<>();
        for (IAServiceProvider provider: providers){
            if (provider.getSupportedServices().contains(service)){
                sps.add(provider);
            }
        }
    }

}
