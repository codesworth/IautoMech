package codesworth.net.iautomech.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import codesworth.net.iautomech.Activities.HomeActivity;
import codesworth.net.iautomech.AdapterViewHolders.RequestHistoryAdapter;
import codesworth.net.iautomech.Model.IARequest;
import codesworth.net.iautomech.R;
import codesworth.net.iautomech.Services.Const;
import codesworth.net.iautomech.Services.DataService;


public class RequestHistoryFrag extends Fragment {

    private static final String ARG_TOW = "tow";
    private RecyclerView recyclerView;
    private RequestHistoryAdapter adapter;
    private ArrayList<IARequest>iaRequests;
    private int type;
    private String uid;
    boolean isTow = false;

    public RequestHistoryFrag() {
        // Required empty public constructor
    }


    public static RequestHistoryFrag newInstance(boolean isTow) {
        RequestHistoryFrag fragment = new RequestHistoryFrag();
        Bundle args = new Bundle();
        args.putBoolean(ARG_TOW,isTow);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            isTow = getArguments().getBoolean(ARG_TOW,false);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_request_history, container, false);
        uid = getActivity().getSharedPreferences(Const.class.getName(),0).getString(Const.USER_UID, "");
        type = getActivity().getSharedPreferences(Const.class.getName(),0).getInt(Const.USER_TYPE, 0);
        iaRequests = new ArrayList<>();
        recyclerView = view.findViewById(R.id.reqrecycler);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        DividerItemDecoration itemDecoration = new DividerItemDecoration(recyclerView.getContext(),manager.getOrientation());
        recyclerView.setLayoutManager(manager);
        recyclerView.addItemDecoration(itemDecoration);
        adapter = new RequestHistoryAdapter(iaRequests,type,getContext());
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(new net.codesworth.gnalet.Services.RecyclerItemClickListener(getContext(), recyclerView, new net.codesworth.gnalet.Services.RecyclerItemClickListener.OnItemClickListener() {
            @Override

            public void onItemClick(View view, int position) {
                IARequest request = iaRequests.get(position);
                ((HomeActivity)getActivity()).openTowDetail(request);
            }

            @Override
            public void onLongItemClick(View view, int position) {

            }
        }));;
        fetchRequests();
        return view;
    }


    void fetchRequests(){

        DataService.getInstance().fetchmyRequests(type,uid,isTow,new DataService.Completion() {
            @Override
            public void didFinish(Object object) {
                if (object.getClass() == ArrayList.class){
                    iaRequests = (ArrayList<IARequest>)object;
                    adapter = new RequestHistoryAdapter(iaRequests,type,getContext());
                    recyclerView.setAdapter(adapter);
                }
            }
        });
    }

}
