package codesworth.net.iautomech.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.Serializable;

import codesworth.net.iautomech.Model.IARequest;
import codesworth.net.iautomech.R;


public class IASPTowFrag extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM_REQUEST = "param1";
    private static final String ARG_PARAM2 = "param2";

    public IARequest request;


    public IASPTowFrag() {
        // Required empty public constructor
    }

    public static IASPTowFrag newInstance(Serializable request) {
        IASPTowFrag fragment = new IASPTowFrag();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM_REQUEST,request);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
          request = (IARequest) getArguments().getSerializable(ARG_PARAM_REQUEST);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_iasptow, container, false);
        return  view;
    }

}
