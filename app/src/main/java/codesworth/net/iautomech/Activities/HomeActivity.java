package codesworth.net.iautomech.Activities;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import codesworth.net.iautomech.Fragments.AutoMech;
import codesworth.net.iautomech.Fragments.HomeFragment;
import codesworth.net.iautomech.Fragments.IAOptionsFrag;
import codesworth.net.iautomech.Fragments.IASPTowFrag;
import codesworth.net.iautomech.Fragments.IATOWFrag;
import codesworth.net.iautomech.Fragments.RequestHistoryFrag;
import codesworth.net.iautomech.Fragments.SettingsFragment;
import codesworth.net.iautomech.Fragments.SignInFragment;
import codesworth.net.iautomech.Fragments.SignUpFragment;
import codesworth.net.iautomech.Fragments.UpdateFragment;
import codesworth.net.iautomech.Model.IARequest;
import codesworth.net.iautomech.Model.IAServiceProvider;
import codesworth.net.iautomech.Model.User;
import codesworth.net.iautomech.R;
import codesworth.net.iautomech.Services.Const;
import codesworth.net.iautomech.Services.DataService;

public class HomeActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    String uid;
    GoogleApiClient googleApiClient;
    final int PERMISSION_LOCATION = 1000;
    public HomeActivityProtocol locationlistener;
    public Location location;
    boolean updateViewInplace = false;
    public IAServiceProvider serviceProvider;
    public User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        setupClient();
        String uid = getSharedPreferences(Const.class.getName(),0).getString(Const.USER_UID, "");
        if (uid == ""){
            SignInFragment fragment = new SignInFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.main_container,fragment).commit();
            return;
        }

        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.main_container);
        if (fragment == null){
            fragment = new HomeFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.main_container,fragment).addToBackStack(null).commit();
        }

        checkForUpdates();
    }



    public void openHome(){
        HomeFragment fragment = new HomeFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.main_container,fragment).addToBackStack(null).commit();
    }




    //**
    // Home Page Activities
    //
    // */

    void setupClient(){
        googleApiClient = new GoogleApiClient.Builder(this).enableAutoManage(this, this).addConnectionCallbacks(this).addApi(LocationServices.API).build();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_LOCATION);
        } else {
            startLocationServices();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        this.location = location;
        if (locationlistener != null){
            locationlistener.didFinishGettingLocation(location);
        }
    }

    @Override
    protected void onStart() {
        googleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }



    public void startLocationServices() {

        try {
            LocationRequest req = LocationRequest.create().setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, req, this);

        } catch (SecurityException e) {
            Log.d("LocationSecurity", "Exception raised at" + e);
            //Toast.makeText(getApplicationContext(),"User Denied Permission")
        }

    }

    public interface HomeActivityProtocol {
        void didFinishGettingLocation(Location location);
    }

    public void setHeadertitle(String headertitle){
        getSupportActionBar().setTitle(headertitle);
    }

    public void openAutoMech(String service, String brand, int radius){

        AutoMech fragment = AutoMech.newInstance(service, brand, radius);
            getSupportFragmentManager().beginTransaction().add(R.id.main_container, fragment).addToBackStack(null).commit();


    }

    public void openSignupFragment(){
        SignUpFragment fragment = new SignUpFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.main_container, fragment).commit();
    }

    public void showUpdateView(String link){
        UpdateFragment fragment = UpdateFragment.newInstance(link);
        getSupportFragmentManager().beginTransaction().add(R.id.main_container, fragment).commit();
    }


    @Override
    public void onBackPressed() {
        if (updateViewInplace){
            return;
        }else {
            if (getSupportFragmentManager().getBackStackEntryCount() > 1){
                super.onBackPressed();
            }
        }
    }

    void checkForUpdates(){
        DataService.getInstance().listenForSignal(new DataService.UpdateProtocol() {
            @Override
            public void eventChanged(boolean changed, String link, long version) {
                if (changed){
                    if (version > Const.APP_VERSION){
                        showUpdateView(link);
                        updateViewInplace = true;

                    }else {
                        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.main_container);
                        if (fragment.getClass() == UpdateFragment.class){
                            getSupportFragmentManager().beginTransaction().remove(fragment).commit();
                            updateViewInplace = false;
                        }
                    }
                }
            }
        });
    }

    public void openOptions(boolean isTowing){
        IAOptionsFrag frag = IAOptionsFrag.newInstance(isTowing);
        getSupportFragmentManager().beginTransaction().add(R.id.main_container,frag).addToBackStack(null).commit();

    }

    public void openTow( String brand, int radius){
        IATOWFrag frag = IATOWFrag.newInstance(brand,radius);
        getSupportFragmentManager().beginTransaction().add(R.id.main_container,frag).addToBackStack(null).commit();
    }

    public void getUserDetails(){
        Object object = DataService.getInstance().readUserInfo(getApplicationContext());
        if (object.getClass() == IAServiceProvider.class){
            serviceProvider = (IAServiceProvider)object;
        }else if (object.getClass() == User.class){
            user = (User)object;
        }
    }

    public void openTowDetail(IARequest request){

        IASPTowFrag frag = IASPTowFrag.newInstance(request);
        getSupportFragmentManager().beginTransaction().add(R.id.main_container,frag).addToBackStack(null).commit();
    }

    public void openSettings(){
        SettingsFragment fragment = new SettingsFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.main_container,fragment).addToBackStack(null).commit();
    }

    public void openSignIn(){
        SignInFragment fragment = new SignInFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.main_container,fragment).commit();
    }

    public void openRequestHistorry(boolean bool){
        RequestHistoryFrag fragment = RequestHistoryFrag.newInstance(bool);
        getSupportFragmentManager().beginTransaction().add(R.id.main_container,fragment).commit();
    }


}
