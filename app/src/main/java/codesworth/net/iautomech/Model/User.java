package codesworth.net.iautomech.Model;

import android.support.annotation.Nullable;

import com.google.firebase.firestore.DocumentSnapshot;

import java.io.Serializable;

import codesworth.net.iautomech.Services.Const;

public class User implements Serializable {

    private String uid;
    private String name;
    private String primaryPhone;
    private String secondaryPhone;
    private String email;
    @Nullable private String imgLink;

    public String getUid() {
        return uid;
    }

    public String getName() {
        return name;
    }

    public String getPrimaryPhone() {
        return primaryPhone;
    }

    public String getSecondaryPhone() {
        return secondaryPhone;
    }

    public String getEmail() {
        return email;
    }

    public String getImgLink() {
        return imgLink;
    }

    public User(String uid, String name, String primaryPhone, String secondaryPhone, String email, @Nullable String imgLink) {
        this.uid = uid;
        this.name = name;
        this.primaryPhone = primaryPhone;
        this.secondaryPhone = secondaryPhone;
        this.email = email;
        this.imgLink = imgLink;
    }

    public User(DocumentSnapshot usersnapshot){
        this.uid = usersnapshot.getId();
        this.name = usersnapshot.getString(Const.USERNAME);
        this.primaryPhone = usersnapshot.getString(Const.PHONE_PRIMARY);
        this.secondaryPhone = usersnapshot.getString(Const.PHONE_SECONDARY);
        this.email = usersnapshot.getString(Const.EMAIL);
        this.email = usersnapshot.getString(Const.LINK);
    }
}
