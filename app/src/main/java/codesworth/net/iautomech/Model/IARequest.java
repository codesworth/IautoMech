package codesworth.net.iautomech.Model;

import com.google.firebase.firestore.DocumentSnapshot;

import java.io.Serializable;
import java.util.Date;

import codesworth.net.iautomech.Services.Const;

public class IARequest implements Serializable {

    private String id;
    private String senderID;
    private String senderName;
    private String serviceProviderID;
    private String carbrand;
    private String servicetype;
    private int status;
    private long timestamp;
    private double fee;
    private String serviceProviderName;

    public String getServiceProviderName() {
        return serviceProviderName;
    }

    public String getSenderName() {
        return senderName;
    }

    public IARequest(String id, String senderID, String serviceProviderID, String carbrand, String servicetype, int status, long timestamp, double fee, String providername, String senderName) {
        this.id = id;
        this.senderID = senderID;
        this.serviceProviderID = serviceProviderID;
        this.carbrand = carbrand;
        this.servicetype = servicetype;
        this.status = status;
        this.timestamp = timestamp;
        this.fee = fee;
        this.senderName = senderName;

        this.serviceProviderName = providername;


    }

    public String getId() {
        return id;
    }

    public String getSenderID() {
        return senderID;
    }

    public String getServiceProviderID() {
        return serviceProviderID;
    }

    public String getCarbrand() {
        return carbrand;
    }

    public String getServicetype() {
        return servicetype;
    }

    public int getStatus() {
        return status;
    }

    public String getDateString(){
       return getDate(this.timestamp);
    }

    public String getCharge(){
        return getCHarge(this.fee);
    }

    public String getStatusString(){
        return setAstatus(this.status);
    }

    public long getTimestamp() {
        return timestamp;
    }

    public double getFee() {
        return fee;
    }




    public IARequest(DocumentSnapshot snapshot){
        this.id = snapshot.getId();
        this.carbrand = snapshot.getString(Const.CAR_BRAND);
        this.fee = snapshot.getDouble(Const._FEE);
        this.senderID = snapshot.getString(Const.SENDER_ID);
        this.serviceProviderID = snapshot.getString(Const.PROVIDER_ID);
        this.servicetype = snapshot.getString(Const.SERVICETYPE);
        this.status = snapshot.getLong(Const.STATUS).intValue();
        this.timestamp = snapshot.getLong(Const._TS);
        this.serviceProviderName = snapshot.getString(Const.PROVIDER_NAME);
    }

    String setAstatus(int status){
        if (status == 1){
            return "PENDING";
        }else if (status == 2){
            return "IN-SERVICE";

        }else if (status == 3){
            return "SERVICED";
        }else {
            return "UNKNOWN";
        }
    }

    String getCHarge(double fee){
        if (fee == 0){
            return "UNDISCLOSED";
        }else{
            return "N "+fee;
        }
    }

    String getDate(long ts){
        Date ndate = new Date(ts);
        return Const.convertStringToDate(ndate);
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
