package codesworth.net.iautomech.Model;

import com.google.firebase.firestore.DocumentSnapshot;

import java.util.ArrayList;

import codesworth.net.iautomech.Services.Const;

public class IAServiceProvider {

    private String id;
    private String name;
    private String email;
    private String contactName;
    private String worskshopAddress;
    private String homeAddress;
    private String currentState;
    private String workshopLocation;
    private String primaryPhone;
    private String secondaryphone;
    private boolean onSpotRepairs;
    private ArrayList<String>supportedVehicles;
    private double longitude;
    private double latitude;

    private ArrayList<String>supportedServices;

    public ArrayList<String> getSupportedServices() {
        return supportedServices;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getContactName() {
        return contactName;
    }

    public String getWorskshopAddress() {
        return worskshopAddress;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public String getCurrentState() {
        return currentState;
    }

    public String getWorkshopLocation() {
        return workshopLocation;
    }

    public String getPrimaryPhone() {
        return primaryPhone;
    }

    public String getSecondaryphone() {
        return secondaryphone;
    }

    public boolean getOnSpotRepairs() {
        return onSpotRepairs;
    }

    public ArrayList<String> getSupportedVehicles() {
        return supportedVehicles;
    }

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public String getEmail() {
        return email;
    }

    public IAServiceProvider(String id, String name, String contactName, String worskshopAddress, String homeAddress, String currentState, String workshopLocation, String primaryPhone, String secondaryphone, ArrayList<String> supportedVehicles, ArrayList<String>supportedServices, boolean onSpotRepairs, double latitude, double longitude) {
        this.id = id;
        this.name = name;
        this.contactName = contactName;
        this.worskshopAddress = worskshopAddress;
        this.homeAddress = homeAddress;
        this.currentState = currentState;
        this.workshopLocation = workshopLocation;
        this.primaryPhone = primaryPhone;
        this.secondaryphone = secondaryphone;
        this.onSpotRepairs = onSpotRepairs;
        this.supportedVehicles = supportedVehicles;
        this.longitude = longitude;
        this.latitude = latitude;

        this.supportedServices = supportedServices;
    }


    public IAServiceProvider(DocumentSnapshot snapshot){
        this.id = snapshot.getId();
        this.name = snapshot.getString(Const.USERNAME);
        this.contactName = snapshot.getString(Const.CONTACTNAME);
        this.worskshopAddress =  snapshot.getString(Const.ADDRESS_WORK);
        this.homeAddress = snapshot.getString(Const.ADDRESS_HOME);
        this.currentState = snapshot.getString(Const.CURRENT_STATE);
        this.workshopLocation = snapshot.getString(Const.LOCATION_WORK);
        this.primaryPhone = snapshot.getString(Const.PHONE_PRIMARY);
        this.secondaryphone = snapshot.getString(Const.PHONE_SECONDARY);
        this.onSpotRepairs = snapshot.getBoolean(Const.ON_SPOT_REP);
        this.supportedVehicles = (ArrayList<String>) snapshot.get(Const.REF_SUPPORTED);
        this.longitude = snapshot.getDouble(Const._LONGITUDE);
        this.latitude = snapshot.getDouble(Const._LATITUDE);
        this.supportedServices = (ArrayList<String>)(snapshot.get(Const.SERVICETYPE));
    }
}
