package codesworth.net.iautomech.Services;

import android.os.AsyncTask;

import com.google.android.gms.maps.GoogleMap;

import java.io.IOException;
import java.util.HashMap;

public class IADirectionData extends AsyncTask<Object,String ,String > {

    GoogleMap googleMap;
    String url;
    String directiondata;
    public AsyncTaskComplete listener;


    @Override
    protected String doInBackground(Object... objects) {
        googleMap = (GoogleMap)objects[0];
        url = (String) objects[1];
        DownloadURL downloadurl = new DownloadURL();

        try {
            directiondata = downloadurl.readUrl(this.url);
        }catch (IOException e){
            e.printStackTrace();
        }
        return directiondata;
    }

    @Override
    protected void onPostExecute(String s) {
        HashMap<String,Object> directionlist = null;
        DataParser parser = new DataParser();
        directionlist = parser.parseDirection(s);
        if (listener != null){
            listener.finishedTask(directionlist);
        }

    }

    public interface AsyncTaskComplete{
        void finishedTask(HashMap<String , Object> result);
    }
}
