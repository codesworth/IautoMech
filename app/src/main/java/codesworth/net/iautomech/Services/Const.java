package codesworth.net.iautomech.Services;

import android.content.Context;
import android.view.View;
import  android.support.design.widget.Snackbar;
//import android.support.design.widget.CoordinatorLayout;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Const {

    //public static final String USERNAME = "
    public static final long APP_VERSION = 1;
    public static final String _VERSION = "version";
    public static final String UPDATABLE = "updatable";
    public static final String APP_LINK = "link";
    public static final String USER_UID = "uid";
    public static final String _ID = "id";
    public static final String USERNAME =  "name";
    public static final String EMAIL = "email";
    public static final String LINK = "link";
    public static final String PROVIDER_NAME = "spname";
    public static final String CONTACTNAME =  "contactName";
    public static final String ADDRESS_WORK = "worskshopAddress";
    public static final String ADDRESS_HOME = "homeAddress";
    public static final String CURRENT_STATE = "currentState";
    public static final String LOCATION_WORK =  "workshopLocation";
    public static final String PHONE_PRIMARY = "primaryPhone";
    public static final String PHONE_SECONDARY = "secondaryphone";
    public static final String ON_SPOT_REP = "onSpotRepairs";
    public static final String REF_SUPPORTED = "supportedVehicles";
    public static final String _LONGITUDE = "longitude";
    public static final String _LATITUDE =  "latitude";
    public static final String SERVICETYPE = "serviceType";
    public static final String  DB_REF_TOKENS = "UserTokens";
    public static final String  TOKEN = "token";
    public static final String REF_USERS = "Users";
    public static final String REF_SERVICE_PROVIDERS = "ServiceProviders";
    public static final String CAR_BRAND = "carBrand";
    public static final String SENDER_ID = "senderID";
    public static final String PROVIDER_ID = "providerID";
    public static final String REQUESTS = "Requests";
    //public static final String SERVICE_TYPE ="servicetype";
    public static final String STATUS = "status";
    public static final String _TS = "timestamp";
    public static final String _FEE = "fee";
    public static final String REF_IAREQUESTS = "IARequests";
    public static final String REF_IAAUTOMECHREQUESTS = "IAServiceAutoRequests";
    public static final String REF_IATOWREQUESTS = "IAServiceTowRequests";
    public static final String JSON_KEY_DURATION = "duration";
    public static final String JSON_KEY_DISTANCE = "distance";
    public static final String JSON_KEY_TEXT = "text";
    public static final String JSON_KEY_VALUE = "value";
    public static final String DIST_VAL = "disval";
    public static final String DUR_VAL = "durval";
    public static final int IS_TYPEOF_PROVIDER = 100;
    public static final int IS_TYPE_USER = 200;
    public static final String USER_TYPE = "usertype";
    /**
     *
     *     private String id;
     private String senderID;
     private String serviceProviderID;
     private String carbrand;
     private String servicetype;
     private int status;
     private long timestamp;
     private double fee;*/

    public static String convertStringToDate(Date indate)
    {
        String dateString = null;
        SimpleDateFormat sdfr = new SimpleDateFormat("dd/MM/yy");
        /*you can also use DateFormat reference instead of SimpleDateFormat
         * like this: DateFormat df = new SimpleDateFormat("dd/MMM/yyyy");
         */
        try{
            dateString = sdfr.format( indate );
        }catch (Exception ex ){
            System.out.println(ex);
        }
        return dateString;
    }


    public static void snackMessage(String message, View view){

        Snackbar.make(view,message,Snackbar.LENGTH_LONG).show();
    }

    public static void showToast(String message, Context context){
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static MaterialDialog makeAlert(String title, String content, Context context, String positiveText){
        MaterialDialog dialog = new MaterialDialog.Builder(context)
                .title(title)
                .content(content)
                .positiveText(positiveText)
                .show();
        return dialog;

    }


    public static MaterialDialog makeLoader(String title, String content, Context context){
        return new MaterialDialog.Builder(context)
                .title( title)
                .content(content)
                .progress(true, 0)
                .show();
    }
}
