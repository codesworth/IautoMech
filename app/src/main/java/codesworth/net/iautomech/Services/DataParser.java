package codesworth.net.iautomech.Services;

import java.util.HashMap;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Priyanka
 */

public class DataParser {

    private HashMap<String, Object> getPlace(JSONObject googlePlaceJson)
    {
        HashMap<String, String> googlePlaceMap = new HashMap<>();
        String placeName = "--NA--";
        String vicinity= "--NA--";
        String latitude= "";
        String longitude="";
        String reference="";

        Log.d("DataParser","jsonobject ="+googlePlaceJson.toString());


        try {
            if (!googlePlaceJson.isNull("name")) {
                placeName = googlePlaceJson.getString("name");
            }
            if (!googlePlaceJson.isNull("vicinity")) {
                vicinity = googlePlaceJson.getString("vicinity");
            }

            latitude = googlePlaceJson.getJSONObject("geometry").getJSONObject("location").getString("lat");
            longitude = googlePlaceJson.getJSONObject("geometry").getJSONObject("location").getString("lng");

            reference = googlePlaceJson.getString("reference");

            googlePlaceMap.put("place_name", placeName);
            googlePlaceMap.put("vicinity", vicinity);
            googlePlaceMap.put("lat", latitude);
            googlePlaceMap.put("lng", longitude);
            googlePlaceMap.put("reference", reference);


        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        return null;

    }
    private List<HashMap<String, String>>getPlaces(JSONArray jsonArray)
    {
        int count = jsonArray.length();
        List<HashMap<String, String>> placelist = new ArrayList<>();
        HashMap<String, String> placeMap = null;

        for(int i = 0; i<count;i++)
        {
            //try {
                placeMap = null ;//getPlace((JSONObject) jsonArray.get(i));
                placelist.add(placeMap);
            //} //catch (JSONException e) {
               // e.printStackTrace();
            //}
        }
        return placelist;
    }

    public List<HashMap<String, String>> parse(String jsonData)
    {
        JSONArray jsonArray = null;
        JSONObject jsonObject;

        Log.d("json data", jsonData);

        try {
            jsonObject = new JSONObject(jsonData);
            jsonArray = jsonObject.getJSONArray("results");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return getPlaces(jsonArray);
    }

    public HashMap<String,Object> parseDirection(String jsondata) {
        JSONArray jsonArray = null;
        JSONObject jsonObject;

        try {
            jsonObject = new JSONObject(jsondata);
            jsonArray = jsonObject.getJSONArray("routes").getJSONObject(0).getJSONArray("legs");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return getDuration(jsonArray);

    }

    private HashMap<String,Object> getDuration(JSONArray jsonArray) {
        HashMap<String,Object> gdirmaps = new HashMap<>();
        try {
            String duration = jsonArray.getJSONObject(0).getJSONObject(Const.JSON_KEY_DURATION).getString(Const.JSON_KEY_TEXT);
            String distance = jsonArray.getJSONObject(0).getJSONObject(Const.JSON_KEY_DISTANCE).getString(Const.JSON_KEY_TEXT);
            Double durvalue = jsonArray.getJSONObject(0).getJSONObject("duration").getDouble(Const.JSON_KEY_VALUE);
            Double distval = jsonArray.getJSONObject(0).getJSONObject(Const.JSON_KEY_DISTANCE).getDouble(Const.JSON_KEY_VALUE);
            gdirmaps.put(Const.JSON_KEY_DURATION, duration);
            gdirmaps.put(Const.JSON_KEY_DISTANCE, distance);
            gdirmaps.put(Const.DUR_VAL, durvalue);
            gdirmaps.put(Const.DIST_VAL,distval);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("GIRDR", "Response ius "+ gdirmaps.toString());
        return gdirmaps;
    }
}
