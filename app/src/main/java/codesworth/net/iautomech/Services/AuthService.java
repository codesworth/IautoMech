package codesworth.net.iautomech.Services;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.iid.FirebaseInstanceId;

public class AuthService {

    private static final AuthService ourInstance = new AuthService();

    public static AuthService auth() {
        return ourInstance;
    }

    private AuthService() {
    }


    public static String getInstanceID(){
        String token = FirebaseInstanceId.getInstance().getToken();
        return token;
    }

    public void signUpWithEmailPass(String email, String password, final DidSignUp finished, final FailedSignIn failed){

        FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password).addOnSuccessListener(new OnSuccessListener<AuthResult>() {
            @Override
            public void onSuccess(final AuthResult authResult) {
                finished.sighupCompleted(authResult.getUser().getUid());
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                failed.failedSignUp(e);
            }
        });
    }

    public void signInWithEmailPass(String email, String password, final Authenticted finished, final FailedSignIn failed){

        FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull final Task<AuthResult> task) {
                if (task.isSuccessful()){
                    DataService.getInstance().getUserTyppe(task.getResult().getUser().getUid(), new DataService.AuthCompletion() {
                        @Override
                        public void onComplete(Object object, int type) {
                            finished.didauthenticate(task.getResult().getUser().getUid(),object,type);
                        }
                    });
                }else {
                    failed.failedSignUp(task.getException());
                }
            }
        });
    }

    public void deleteUser(final String uid, final Callback callback){
        FirebaseAuth.getInstance().getCurrentUser().delete().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){

                }
            }
        });
    }

    public void signedOut(){
        FirebaseAuth.getInstance().signOut();
    }

    public interface DidSignUp{
        void sighupCompleted(String uid);
    }

    public interface Authenticted{
        void didauthenticate(String uid, Object object,int type);
    }

    public interface FailedSignIn{
        void failedSignUp(Exception e);
    }

    public interface Callback{
        void callback(boolean e);
    }
}
