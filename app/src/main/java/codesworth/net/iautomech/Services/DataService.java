package codesworth.net.iautomech.Services;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.SetOptions;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import codesworth.net.iautomech.Model.IARequest;
import codesworth.net.iautomech.Model.IAServiceProvider;
import codesworth.net.iautomech.Model.User;

public class DataService {
    private static final DataService ourInstance = new DataService();

    public static DataService getInstance() {
        return ourInstance;
    }
    private static final String  FILE_STORAGE_KEY = "iauromechdatakey";

    private int IAREQUEST_COUNTER = 0;
    private DataService() {
    }

    public DatabaseReference getGeoRef(){
        return FirebaseDatabase.getInstance().getReference("Locations");
    }
    @NonNull
    private FirebaseFirestore rootStore(){
        return FirebaseFirestore.getInstance();
    }

    private DatabaseReference rootbase(){
        return FirebaseDatabase.getInstance().getReference();
    }

    private CollectionReference userRefs(){
        return rootStore().collection(Const.REF_USERS);
    }

    private CollectionReference userTokenrefs(){
        return  rootStore().collection(Const.DB_REF_TOKENS);
    }
    private CollectionReference serviceProviderRefs(){
        return  rootStore().collection(Const.REF_SERVICE_PROVIDERS);
    }

    private CollectionReference userIARequestRef(String uid){
        return userRefs().document(uid).collection(Const.REF_IAREQUESTS);
    }

    private CollectionReference providerIAAutomechRequestRef(String uid){
        return serviceProviderRefs().document(uid).collection(Const.REF_IAAUTOMECHREQUESTS);
    }
    private CollectionReference providerIATowRequestRef(String uid){
        return serviceProviderRefs().document(uid).collection(Const.REF_IATOWREQUESTS);
    }

    public String getDocumentID(){
        return requestRefs().document().getId();
    }

    private CollectionReference requestRefs(){
        return  rootStore().collection(Const.REF_IAREQUESTS);
    }


    public void createUser(User user, final CompletionHandler handler){

        HashMap<String, Object>data = new HashMap<>();
        data.put(Const.USERNAME, user.getName());
        data.put(Const.EMAIL, user.getEmail());
        data.put(Const.PHONE_PRIMARY,user.getPrimaryPhone());
        data.put(Const.PHONE_SECONDARY, user.getSecondaryPhone());
        data.put(Const.LINK, user.getImgLink());

        userRefs().document(user.getUid()).set(data, SetOptions.merge()).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                handler.completed(task.isSuccessful());
            }
        });
    }

    public void getUserTyppe(final String uid, final AuthCompletion completion){
        userRefs().document(uid).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.getResult().exists()){
                    User user = new User(task.getResult());
                    completion.onComplete(user,Const.IS_TYPE_USER);
                }else{
                    serviceProviderRefs().document(uid).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentSnapshot> ptask) {
                            if (ptask.getResult().exists()){
                                IAServiceProvider provider = new IAServiceProvider(ptask.getResult());
                                completion.onComplete(provider,Const.IS_TYPEOF_PROVIDER);

                            }
                        }
                    });
                }
            }
        });
    }

    public void createServiceProvider(IAServiceProvider provider, final CompletionHandler handler){
        HashMap<String, Object>data = new HashMap<>();

        data.put(Const.USERNAME, provider.getName());
        data.put(Const.CONTACTNAME, provider.getContactName());
        data.put(Const.ADDRESS_WORK, provider.getWorskshopAddress());
        data.put(Const.ADDRESS_HOME, provider.getHomeAddress());
        data.put(Const.LOCATION_WORK, provider.getWorkshopLocation());
        data.put(Const.CURRENT_STATE, provider.getCurrentState());
        data.put(Const.PHONE_PRIMARY, provider.getPrimaryPhone());
        data.put(Const.PHONE_SECONDARY, provider.getSecondaryphone());
        data.put(Const.REF_SUPPORTED, provider.getSupportedVehicles());
        data.put(Const.ON_SPOT_REP, provider.getOnSpotRepairs());
        data.put(Const.SERVICETYPE, provider.getSupportedServices());
        data.put(Const.EMAIL, provider.getEmail());
        data.put(Const._LATITUDE, provider.getLatitude());
        data.put(Const._LONGITUDE, provider.getLongitude());

        serviceProviderRefs().document(provider.getId()).set(data, SetOptions.merge()).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

                handler.completed(task.isSuccessful());
            }
        });
    }

    public interface CompletionHandler{
        void completed(boolean succesfully);
    }

    public void listenForSignal(final UpdateProtocol oncompletion){
        rootStore().collection("CS_KILL").document("UPDATEFIELD").addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot snapshot, @Nullable FirebaseFirestoreException e) {
                if (snapshot.exists()){
                    boolean update = snapshot.getBoolean(Const.UPDATABLE);
                    if (update){
                        String link = snapshot.getString(Const.APP_LINK);
                        Long version = snapshot.getLong(Const._VERSION);
                        oncompletion.eventChanged(update,link,version);
                    }else {
                        oncompletion.eventChanged(update,"",0);
                    }
                }
            }
        });
    }

    public void fetchServiceProvider(String key, final Completion onFinish){
        serviceProviderRefs().document(key).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.getResult().exists()){
                    IAServiceProvider provider = new IAServiceProvider(task.getResult());
                    onFinish.didFinish(provider);
                }
            }
        });
    }

    public void placeRequest(final IARequest request, final String uid, final CompletionHandler handler, final boolean isTow){
        HashMap<String, Object>data = new HashMap<>();
        data.put(Const._TS, request.getTimestamp());
        data.put(Const.SENDER_ID, request.getSenderID());
        data.put(Const.PROVIDER_ID,request.getServiceProviderID());
        data.put(Const.SERVICETYPE, request.getServicetype());
        data.put(Const.CAR_BRAND, request.getCarbrand());
        data.put(Const.STATUS,request.getStatus());
        data.put(Const._FEE,request.getFee());

        requestRefs().document(request.getId()).set(data,SetOptions.merge()).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                final HashMap map = new HashMap();
                map.put(Const._TS, request.getTimestamp());
                userIARequestRef(uid).document(request.getId()).set(map, SetOptions.merge()).addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if (isTow){
                            providerIATowRequestRef(request.getServiceProviderID()).document(request.getId()).set(map, SetOptions.merge()).addOnCompleteListener(new OnCompleteListener() {
                                @Override
                                public void onComplete(@NonNull Task task) {
                                    handler.completed(task.isSuccessful());
                                }
                            });
                        }else {
                            providerIAAutomechRequestRef(request.getServiceProviderID()).document(request.getId()).set(map, SetOptions.merge()).addOnCompleteListener(new OnCompleteListener() {
                                @Override
                                public void onComplete(@NonNull Task task) {
                                    handler.completed(task.isSuccessful());
                                }
                            });
                        }

                    }
                });
            }
        });
    }

    public void fetchmyRequests(int type, String uid,boolean isTow, final Completion handler){
        final ArrayList<IARequest>requests = new ArrayList<>();
        CollectionReference aref;
        if (type == Const.IS_TYPEOF_PROVIDER){
            if (isTow){
                aref = providerIATowRequestRef(uid);
            }else {
                aref = providerIAAutomechRequestRef(uid);
            }
        }else if (type == Const.IS_TYPE_USER){
            aref = userIARequestRef(uid);
        }else{
            return;
        }
        IAREQUEST_COUNTER = 0;
        aref.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull final Task<QuerySnapshot> task) {
                if (task.getResult() != null){
                    for (DocumentSnapshot snap: task.getResult().getDocuments()){
                        requestRefs().document(snap.getId()).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<DocumentSnapshot> newtask) {
                                IAREQUEST_COUNTER++;
                                if (newtask.getResult() != null){
                                    IARequest request = new IARequest(newtask.getResult());
                                    requests.add(request);
                                    if (IAREQUEST_COUNTER == task.getResult().size()){
                                        handler.didFinish(request);
                                    }
                                }
                            }
                        });
                    }
                }
            }
        });
    }


    public interface AuthCompletion{
        void onComplete(Object object,int type);
    }

    public interface Completion{
        void didFinish(Object object);
    }

    public interface UpdateProtocol{
        void eventChanged(boolean changed,String link, long version);
    }



    public static void writeObject(Context context, Object object) throws IOException {
        FileOutputStream fos = context.openFileOutput(FILE_STORAGE_KEY, Context.MODE_PRIVATE);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(object);
        oos.close();
        fos.close();
    }

    public static Object readObject(Context context) throws IOException,
            ClassNotFoundException {
        FileInputStream fis = context.openFileInput(FILE_STORAGE_KEY);
        ObjectInputStream ois = new ObjectInputStream(fis);
        Object object = ois.readObject();
        return object;
    }

    public Object readUserInfo(Context context){
        Object object = null;
        try {
            object = readObject(context);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return object;
    }

    public void changeRequestStatus(IARequest request, final RequestStatusChanged handler){
        final int newstatus = request.getStatus() + 1;
        requestRefs().document(request.getId()).update(Const.STATUS,newstatus).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                handler.didFinishChangingStatus(task.isSuccessful(),newstatus);
            }
        });
    }

    public interface RequestStatusChanged{
        void didFinishChangingStatus(boolean status, int newStatus);
    }

}
